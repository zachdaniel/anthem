#![feature(slice_patterns)]
#![feature(box_patterns)]
#[macro_use]
extern crate nom;

mod lexer;
mod compiler;
mod parser;

fn main() {
	let lexed = lexer::lex("x = 5\n main = (+ x 5)\n".as_bytes());
	println!("LEXED CODE \n");
	println!("{:?}", lexed);
	let parsed = parser::parse(lexed);
    println!("PARSED CODE \n");
    println!("{:?}", parsed);
    let status = compiler::compile(parsed);

   	println!("COMPILED CODE \n");
   	println!("{:?}", status);
}
