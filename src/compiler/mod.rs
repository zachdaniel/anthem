extern crate llvm;
use self::llvm::*;

use parser::Node;
use lexer::Term;
// use std::path::Path;

pub fn compile(expr: Vec<Node>) -> u8 {
	let mut expr_iter = expr.into_iter();
	let context = Context::new();
	let builder = Builder::new(&*context);
	let module = Module::new("Thing", &*context);
	while let Some(node) = expr_iter.next() {
		node.compile(&context, &module, &builder);
	}
	module.verify().unwrap();
	module.write_bitcode("/Users/zachdaniel/Development/anthem/anthem.ll");
	println!("{:?}", module);
	1
}

// pub enum Node {
// 	Binding(String, Box<Node>),
// 	Expression(Box<Vec<Node>>),
// 	Conditional(Box<Node>, Box<Node>, Box<Vec<Node>>, Option<Box<Vec<Node>>>),
// 	Value(Term)
// }

impl Node {
	fn compile<'a>(self, context: &'a Context, module: &'a Module, builder: &'a Builder) -> &'a Value {
		// if arity is zero?
		match self {
			Node::Binding(name, box node) => {
				println!("defining {}", name);
				let func = module.add_function(&name, Type::get::<fn() -> u64>(context));
				let block = func.append("entry");
				builder.position_at_end(block);
				return Node::return_value(builder, node.compile(context, module, builder))
			},
			Node::Value(term) => {
				term.compile(context, module, builder)
			},
			Node::Expression(box nodes) => {
				nodes.into_iter().map(|x| x.compile(context, module, builder)).collect::<Vec<&Value>>().pop().unwrap()
			}
			_ => panic!()
		}
	}

	fn return_value<'a>(builder: &'a Builder, t: &'a Value) -> &'a Value {
		builder.build_ret(&t)
	}
}

impl Term {
	fn compile<'a>(self, context: &'a Context, module: &'a Module, builder: &'a Builder) -> &'a Value {
		match self {
			Term::Reference(ref st) => {
				return builder.build_call(module.get_function(st).unwrap(), &[])
			},
			Term::Integer(i) => {
				return i.compile(context)
			},
			_ => panic!()
		}
	}
}