use lexer::{Expression, Term};
use nom::{IResult, Err, ErrorKind};

#[derive(Debug)]
pub enum Node {
	Binding(String, Box<Node>),
	Expression(Box<Vec<Node>>),
	Call(Term, Box<Vec<Node>>),
	Conditional(Box<Node>, Box<Node>, Box<Vec<Node>>, Option<Box<Vec<Node>>>),
	Value(Term)
}

impl Node {
	pub fn binding(string: String, expression: Expression) -> Node {
		Node::Binding(string, 
			Box::new(
				Node::expression(expression)
			)
		)
	}

	pub fn value(string: Term) -> Node {
		Node::Value(string)
	}

	pub fn expression(exprs: Expression) -> Node {
		parse_single(exprs)
	}

	pub fn conditional(conditional: Vec<Expression>, consequence: Vec<Expression>, elsifs: Vec<Node>, el: Option<Vec<Expression>>) -> Node {
		Node::Conditional(
			Box::new(Node::Expression(Box::new(parse(conditional)))),
			Box::new(Node::Expression(Box::new(parse(consequence)))),
			Box::new(elsifs),
			el.map(|x| Box::new(parse(x)))
		)
	}
}

named!(single_expr <&[Expression], Node>,
	alt!(
		binding |
		call |
		conditional |
		value |
		expr
	)
);

named!(program <&[Expression], Vec<Node> >,
	many1!(single_expr)
);

fn conditional(i: &[Expression]) -> IResult<&[Expression], Node> {
	match i {
		[Expression::Term(Term::If), Expression::Expression(ref conditional), Expression::Expression(ref consequence), r..] => {
			let mut elsifs = Vec::new();
			let mut rest = r;
			let mut el = None;
			while let [Expression::Term(Term::Elsif),Expression::Expression(ref conditional2),Expression::Expression(ref consequence2),r2..] = rest {
			  	elsifs.push(Node::conditional(*conditional2.clone(), *consequence2.clone(), Vec::new(), None));
			  	rest = r2;
		    }
		    if let [Expression::Term(Term::Else),Expression::Expression(ref consequence),r2..] = rest {
	    		el = Some(consequence);
	    		rest = r2;
	    	}
	    	IResult::Done(rest, Node::conditional(*conditional.clone(), *consequence.clone(), elsifs, el.map(|x| *x.clone())))
		},
		_ => IResult::Error(Err::Code(ErrorKind::Custom(105)))
	}
}

fn expr(i: &[Expression]) -> IResult<&[Expression], Node > {
	match i {
		[Expression::Term(term),rest..] => {
			IResult::Node::Call(term, rest)
		},
		_ => IResult::Error(Err::Code(ErrorKind::Custom(106)))
	}
}

fn binding(i: &[Expression]) -> IResult<&[Expression], Node > {
	match i {
		[ Expression::Term(Term::Reference(ref x)), 
		  Expression::Term(Term::Binds), 
		  ref z, 
		  rest..] => IResult::Done(rest, Node::binding(x.clone(), z.clone())),
		_ => IResult::Error(Err::Code(ErrorKind::Custom(101)))
	}
}

fn value(i: &[Expression]) -> IResult<&[Expression], Node > {
	match i {
		[Expression::Term(ref x),rest..] => IResult::Done(rest, Node::value(x.clone())),
		_          => IResult::Error(Err::Code(ErrorKind::Custom(102)))
	}
}

pub fn parse_single(i: Expression) -> Node {
	match single_expr(&[i]) {
		IResult::Done(_, x) => x,
		_ => panic!()
	}
}

pub fn parse(i: Vec<Expression>) -> Vec<Node> {
	    match program(i.as_slice()) {
    	IResult::Done(_, x) => x,
    	_       => panic!()
    }
}