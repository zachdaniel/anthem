use nom::{multispace, IResult, Err, ErrorKind};
use std::str;
use std::fmt;

named!(term <Expression>,
	chain!(
		text: take_while1!(is_not_whitespace),
		||  { return Expression::term(text) }
	)
);

named!(parens <Expression>,
	chain!(
		tag!("(") ~
		sub: is_not!(")") ~
		tag!(")"),
		|| {
			return Expression::expression(Box::new(lex(sub)))
		}
	)
);

named!(integer <Expression>,
    chain!(
        num: is_a!("123456789"),
        || {
            return Expression::Term(Term::integer(num))
        }
    )
);

named!(expr <Expression>,
  	alt_complete!(
        expr_semicolon |
        paren_expr |
  		parens |
        integer |
  		term
	)
);

named!(expression <Expression>,
	delimited!(opt!(multispace), expr, opt!(multispace))
);

named!(program <Vec<Expression> >,
	many1!(expression)
);

fn is_not_whitespace(c: u8) -> bool {
    c as char != ' ' && c as char != '\t' && c as char != '\n'
}

fn expr_semicolon<'a>(input: &[u8]) -> IResult<&[u8], Expression> {
    if input.len() == 0 {
        return IResult::Error(Err::Code(ErrorKind::Custom(205)));
    }

    let mut in_a_comment = false;
    let mut counter = 0;

    for idx in 0..input.len() {
        if input[idx] == '#' as u8 {
            in_a_comment = true;
        } else if input[idx] == '\r' as u8 || input[idx] == '\n' as u8 {
            in_a_comment = false;
        } else if input[idx] == '(' as u8 {
            counter += 1;
        } else if input[idx] == ')' as u8 {
            counter -= 1;
        } else if input[idx] == '\n' as u8 || input[idx] == ';' as u8 && counter == 0 && !in_a_comment {
            return IResult::Done(&input[(idx + 1)..], Expression::Expression(Box::new(lex(&input[1..idx]))));
        } else if input[idx] == ';' as u8 && !in_a_comment && counter == 0{

        }
    }
    IResult::Error(Err::Code(ErrorKind::Custom(202)))
}

fn paren_expr<'a>(input: &[u8]) -> IResult<&[u8], Expression> {
    if input.len() == 0 {
        return IResult::Error(Err::Code(ErrorKind::Custom(203)));
    }

    if input[0] != '(' as u8 {
        return IResult::Error(Err::Code(ErrorKind::Custom(204)));
    }

    let mut in_a_comment = false;
    let mut counter = 1;

    for idx in 1..input.len() {
        if input[idx] == '#' as u8 {
            in_a_comment = true;
        } else if input[idx] == '\r' as u8 || input[idx] == '\n' as u8 {
            in_a_comment = false;
        } else if input[idx] == '(' as u8 {
            counter += 1;
        } else if input[idx] == ')' as u8 {
            counter -= 1;
            if !in_a_comment && counter == 0 {
                return IResult::Done(&input[(idx + 1)..], Expression::Expression(Box::new(lex(&input[1..idx]))));
            }
        }
    }

    IResult::Error(Err::Code(ErrorKind::Custom(201)))
}

#[derive(Clone)]
pub enum Expression {
  	Expression(Box<Vec<Expression>>),
  	Term(Term)
}

#[derive(Debug, Clone)]
pub enum Term {
    Reference(String),
    Integer(i64),
    Binds,
    If,
    Elsif,
    Else,
    PrimFn(PrimFn)
}

#[derive(Debug, Clone)]
pub enum PrimFn {
    Add,
    Subtract,
    Multiply,
    Divide
}

impl Term {
    pub fn from(text:&[u8]) -> Term {
        let str_text = str::from_utf8(text).unwrap();
        match str_text {
            "=" => Term::Binds,
            "if" => Term::If,
            "elsif" => Term::Elsif,
            "else" => Term::Else,
            "+" => Term::PrimFn(PrimFn::Add),
            _   => Term::Reference(str_text.to_string())
        }
    }

    pub fn integer(text: &[u8]) -> Term {
        let str_text = str::from_utf8(text).unwrap();
        Term::Integer(str_text.parse::<i64>().unwrap())
    }
}

impl Expression {
	pub fn term(text:&[u8]) -> Expression {
		Expression::Term(
            Term::from(text)
		)
	}

	pub fn expression(exprs: Box<Vec<Expression>>) -> Expression {
		Expression::Expression(exprs)
	}
}


impl fmt::Debug for Expression {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
    	match self {
    		&Expression::Expression(ref vec) => {
    			write!(f, "( {:?} )", vec)
    		},
    		&Expression::Term(ref term) => {
    			write!(f, "{:?}", term)
    		}
    	}
    }
}


pub fn lex(text: &[u8]) -> Vec<Expression> {
    match program(text) {
    	IResult::Done(_, x) => x,
    	_       => panic!()
    }
}